import os
import sys

ip = os.environ.get('IP', '0.0.0.0')
port = int(os.environ.get('PORT', 3000))
root_path = os.path.dirname(sys.modules['__main__'].__file__)
env = os.environ.get('ENV', 'dev')

# GENERAL SETUP

general = dict(
    company = "AISoft",
    token_secret = "8QdNZeLLi3clhTOVkhFpy0oNIYl1qmWQcUDQnI1kLr7RcRJVmhoCd1qWASTRoJK"
)

defaultFileStrategy = "s3"

server = dict(
    debug = True,
    host = ip,
    port = port,
    url = "http://publer.herokuapp.com:%s" % port,
    fileStrategy = "fake" if env == "test" else defaultFileStrategy,
    allowDML = False,
    defaultAcl = "11"
)

# DATABASE SETUP

database = {
    "dev": dict(
        host = "neo4j",
        port = 7474,
        username = "neo4j",
        password = "neo4j"
    ),
    "test": dict(
        host = "neo4j",
        port = 7474,
        username = "neo4j",
        password = "neo4j"
    )
}[env]

# EMAIL SETUP

smtp = dict (
    sender = "puber@aisoft.com.br",
    host = "smtp.gmail.com",
    port = 587,
    username = "puber@aisoft.com.br",
    password = "night123[]0"
)

mailgun = dict(
    sender = "suport@aisoft.com.br",
    url = "https://api.mailgun.net/v3/mg.aisoft.com.br/messages",
    key = "6ecdb08a33863aa0a2788226960671d7-49a2671e-8c38ed2f"
)

# STORAGE SETUP

dropbox = dict(
    token = "Fua0VA87Py4AAAAAAAANF0AoaSUInvM6Rr4Ex3MdPZwcX_ncPQIaBtRaa7XtFiN5"
)

s3 = dict(
    access = "AKIAIPRMLSLHJFVKLD7Q",
    secret = "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",
    bucket = "neo4j-baas",
    region = "us-east-2",
    host = "https://s3.us-east-2.amazonaws.com/neo4j-baas"
)

# SOCIAL SETUP

facebook = dict(
    app_id = "",
    app_secret = ""
)
