from flask import abort, json
from daos.data_dao import DataDao
from lib.register import AppRegister
from decorators.secured import Secured
from db.query import Transactional

@Transactional
def SimpleService(tx):
  return 'example service'

AppRegister.registerService(route='/api/service/example', callback=SimpleService)