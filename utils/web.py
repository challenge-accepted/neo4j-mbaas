import requests

class WebUtils():

  @staticmethod
  def get(url, data, headers={'content-type': 'application/json'}):
    req = requests.get(url, params=data, headers=headers)

    result = req.json()

    return result