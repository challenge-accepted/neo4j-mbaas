from utils.file.drive import GDrive
from utils.file.dropbox import Dropbox
from utils.file.s3 import S3
from utils.file.fake import FakeDrive
import settings

class FileUtils():

    @staticmethod
    def getDrive(strategy=settings.server['fileStrategy']):
        drive = None

        if strategy == 'googledrive':
            drive = GDrive()
        elif strategy == 'dropbox':
            drive = Dropbox()
        elif strategy == 's3':
            drive = S3()
        elif strategy == 'fake':
            drive = FakeDrive()
        else:
            raise Exception('No valid strategy specified!')

        return drive