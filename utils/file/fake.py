from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

class FakeDrive():

    def upload(self, fileName, content=None, uri=None):
        return dict(
            fileId = 0,
            fileUrl = ''
        )

    def delete(self, id):
        pass

    def update(self, id, content=None, uri=None):
        pass