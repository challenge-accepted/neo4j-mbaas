import boto
import boto.s3.connection
from boto.s3.key import Key
import settings

class S3():
    drive = None
    
    def __init__(self):
        if S3.drive is None:
            # Create S3 instance
            S3.drive = boto.s3.connect_to_region(
              settings.s3['region'],
              aws_access_key_id = settings.s3['access'], 
              aws_secret_access_key = settings.s3['secret'],
              is_secure = False,
              calling_format = boto.s3.connection.OrdinaryCallingFormat()
            )

            # Set default bucket
        self.bucket = S3.drive.get_bucket(settings.s3['bucket'])

    def upload(self, fileName, content=None, uri=None):        
        if uri is not None:
            in_file = open(uri, "rb")
            content = in_file.read()
            in_file.close()
        elif content is not None:
            # converts to bytes
            content = str.encode(content)
        
        key = self.bucket.new_key(fileName)        
        key.set_contents_from_string(content)
        key.set_acl('public-read')

        return dict(
            fileId = fileName,
            fileUrl = "%s/%s" % (settings.s3['host'], fileName)
        )

    def delete(self, id):
        key = Key(self.bucket)
        key.key = id
        key.delete()

    def update(self, id, content=None, uri=None):
        pass

