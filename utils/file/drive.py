from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

class GDrive():
    drive = None

    def __init__(self):
        if GDrive.drive is None:
            gauth = GoogleAuth()

            # Create GoogleDrive instance with authenticated GoogleAuth instance.
            GDrive.drive = GoogleDrive(gauth)

    def upload(self, fileName, content=None, uri=None):
        # Create GoogleDriveFile instance with title 'Hello.txt'.
        file = GDrive.drive.CreateFile({'title': fileName})

        # Set content
        if content is not None:
            file.SetContentString(content)
        elif uri is not None:
            file.SetContentFile(uri)

        file.Upload()

        # Share file
        file.InsertPermission({
                        'type': 'anyone',
                        'value': 'anyone',
                        'role': 'reader',
                        'withLink': True})

        return dict(
            fileId = file['id'],
            fileUrl = 'https://drive.google.com/uc?export=download&id=%s' % file['id']
        )

    def delete(self, id):
        file = GDrive.drive.CreateFile({'id': id})
        file.Delete()

    def update(self, id, content=None, uri=None):
        file = GDrive.drive.CreateFile({'id': id})

        # Set content
        if content is not None:
            file.SetContentString(content)
        elif uri is not None:
            file.SetContentFile(uri)

        file.Upload()

