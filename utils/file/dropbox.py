import dropbox
import settings

class Dropbox():
    drive = None
    
    def __init__(self):
        if Dropbox.drive is None:
            # Create Dropbox instance.
            Dropbox.drive = dropbox.Dropbox(settings.dropbox['token'])

    def upload(self, fileName, content=None, uri=None):        
        if uri is not None:
            in_file = open(uri, "rb")
            content = in_file.read()
            in_file.close()
        elif content is not None:
            # converts to bytes
            content = str.encode(content)
        
        file = Dropbox.drive.files_upload(content, path="/%s" % fileName, autorename=True)
        share = Dropbox.drive.sharing_create_shared_link(file.path_display)

        return dict(
            fileId = fileName,
            fileUrl = share.url.replace("dl=0", "dl=1")
        )

    def delete(self, id):
        Dropbox.drive.files_delete("/%s" % id)

    def update(self, id, content=None, uri=None):
        pass

