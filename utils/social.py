import requests
from .web import WebUtils
from settings import facebook

class SocialUtils():

  @staticmethod
  def validateGoogle(token):
    url = 'https://www.googleapis.com/oauth2/v3/tokeninfo'

    validation = WebUtils.get(url=url, data={"id_token": token})    

    try:
      userid = validation['sub']

      return userid
    except KeyError:
      return False

  @staticmethod
  def validateFacebook(token):
    url = 'https://graph.accountkit.com/debug_token'
    access_token = "%s|%s" % (facebook['app_id'], facebook['app_secret'])

    validation = WebUtils.get(url=url, data={"input_token": token, "access_token": access_token})    

    try:
      userid = validation['id']

      return userid
    except KeyError:
      return False
    pass