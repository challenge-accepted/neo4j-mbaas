import re


class DaoUtils():

    @staticmethod
    def serializeObject(root):
        serialized = dict()
        fields = []

        for field in root:
            if isinstance(root[field], dict):
                inner = root[field]

                for key in inner:
                    serialized['%s__%s' % (field, key)] = inner[key]

                # save field to remove later
                fields.append(field)

        # remove serialized fields
        for field in fields:
            del root[field]

        root.update(serialized)
            

    @staticmethod
    def unserializeObject(root):
        unserialized = dict()
        fields = []
        pattern = '(.*)__(.*)'
        
        for field in root:
            if re.match(pattern, field):
                matches = re.search(pattern, field)
                inner = matches.group(1)
                key = matches.group(2)

                # initialize field if needed
                if inner not in unserialized:
                    unserialized[inner] = dict()

                # unserialize field to inner object
                unserialized[inner][key] = root[field]

                # save field toremove later
                fields.append(field)

        # remove serialized fields
        for field in fields:
            del root[field]

        root.update(unserialized)

    @staticmethod
    def to_dict(record):
        dest = {}

        for key in record.keys():
            dest[key] = record.get(key)

        return dest