import settings
import smtplib
import time
from exceptions.network import NetworkException
import requests

class EmailUtils():

    @staticmethod
    def _send_email(recipient, subject, body):
        FROM = settings.smtp['sender']
        TO = recipient if type(recipient) is list else [recipient]
        SUBJECT = subject
        TEXT = body

        # Prepare actual message
        message = """From: %s\nTo: %s\nSubject: %s\n\n%s
        """ % (FROM, ", ".join(TO), SUBJECT, TEXT)

        try:
            server = smtplib.SMTP(settings.smtp['host'], settings.smtp['port'])
            server.ehlo()
            server.starttls()
            server.login(settings.smtp['username'], settings.smtp['password'])
            server.sendmail(FROM, TO, message)
            server.close()
        except Exception as ex:
            print(ex)
            raise NetworkException()

    @staticmethod
    def send_email(recipient, subject, body):
        config = settings.mailgun

        try:
            requests.post(
                config.get('url'),
                auth=("api", config.get('key')),
                data={"from": config.get('sender'),
                    "to": [recipient],
                    "subject": subject,
                    "text": body})
        except Exception as ex:
            print(ex)
            raise NetworkException()
