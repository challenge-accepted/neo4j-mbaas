import time
import json

class Utils:

    @staticmethod
    def isEmpty(model, field):
        if field not in model or model[field] is None:
            return True
        else:
            value = model[field]
              
            if isinstance(value, str):
                return len(value.strip()) == 0
            elif isinstance(value, list):
                return len(value) == 0
            else: 
                try:
                    value = float(value)

                    if float(value) == 0:
                        return True
                except:
                    return True

        return False 

    @staticmethod
    def now():
        return int(round(time.time() * 1000))

    @staticmethod
    def update(dest, multidict_source):
        for key in multidict_source.keys():
            value = multidict_source[key]
            if isinstance(value, list):
                value = value[0]
            
            try:
                dest[key] = int(value)
            except:
                dest[key] = value

    @staticmethod
    def dict_normalize(valuesDict, fieldsList):
        SUFIX = '_clean'
        result = dict()

        for key in valuesDict:
            if key in fieldsList:
                result[key + SUFIX] = Utils.normalize(valuesDict[key])

            result[key] = valuesDict[key]

        # return normalized values 
        return result

    @staticmethod
    def create_body(body):
        embed = {}

        def delete(*keys):
            for key in keys:
                if key in body:
                    del body[key]
                # save and remove any embed dictionary content
                elif isinstance(body[key], dict):
                    embed[key] = body[key]
                    del body[key]

        # clean up
        delete('id', 'date_create', 'creator')

        # sets data
        body['date_create'] = Utils.now()

        return embed
    
    @staticmethod
    def update_body(body):
        embed = {}
        def delete(*keys):
            for key in keys:
                if key in body:
                    del body[key]
                # save and remove any embed dictionary content
                elif isinstance(body[key], dict):
                    embed[key] = body[key]
                    del body[key]

        # clean up
        delete('id', 'date_create', 'creator', 'labels', 'ins', 'outs')

        # updates data
        body['date_update'] = Utils.now()

        return embed

    @staticmethod
    def to_json(payload):
        return json.JSONEncoder().encode(payload)

    @staticmethod
    def from_json(payload):
        return json.JSONDecoder().decode( str(payload, 'utf8') )