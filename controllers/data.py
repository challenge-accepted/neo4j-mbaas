import re
from flask import request, abort, jsonify
from decorators.secured import OptionalSecured, Secured
from controllers.result import Result
from exceptions.conflict import ConflictException
from exceptions.unauthorized import UnauthorizedException
from lib.register import AppRegister
from utils.utils import Utils
from ogm.node import Node
from decorators.uploader import Uploader
import settings

def Controller(app):
    baseApiUrl = "/api/v1/data"

    def assertSafe(labels):
        """
            Asserts user is not calling any protected resource from the generic endpoint
        """
        
        unsafes = ['USER', 'PROFILE', 'GROUP']

        if not len([label for label in labels.split(",") if label.upper() in unsafes]) == 0:
            abort(400, "You can't call this label from this endpoint.")

    @app.route("%s/cypher" % baseApiUrl, methods=['POST'])
    @Secured
    def cypher(currentUser):
        body = dict(
            parameters = {}
        );
        
        body.update(request.json)

        if not body.get('statement'):
            abort(400, 'body statement value required')

        # check safety
        if not settings.server.get('allowDML', False):
            if re.search(r'(create)|(merge)|(set)|(detach)|(delete)', body.get('statement').lower()):
                abort(403, 'cypher statement must only contains search clauses')

        node = Node(currentUser)

        result = node.run(query=body.get('statement'), parameters=body.get('parameters'))

        return jsonify(result)

    @app.route("%s/<string:labels>" % baseApiUrl, methods=['GET'])
    @OptionalSecured
    def fetch_all(currentUser, labels):
        assertSafe(labels)

        args = dict(
            limit = 10,
            skip = 0,
            filter = "true",
            sort=None
        )
        labels = [label.strip().upper() for label in labels.split(',')]

        Utils.update(args, request.args)

        node = Node(currentUser)

        result = node.filter(labels=labels, filter=args.get('filter'), sort=args.get('sort'), skip=args['skip'], limit=args['limit'])
        
        return jsonify(result)

    @app.route("%s/<string:labels>/<int:id>" % baseApiUrl, methods=['GET'])
    @OptionalSecured
    def fetch(currentUser, labels, id):
        assertSafe(labels)

        node = Node(currentUser, id=id)

        result = node.fetch()
        
        return jsonify(result)

    @app.route("%s/<string:labels>/<int:id>/ins" % baseApiUrl, methods=['GET'])
    @app.route("%s/<string:labels>/<int:id>/ins/<string:label>" % baseApiUrl, methods=['GET'])
    @OptionalSecured
    def fetchIns(currentUser, labels, id, label=None):
        assertSafe(labels)

        args = dict(
            limit = 10,
            skip = 0,
            filter="true",
            sort=None
        )

        Utils.update(args, request.args)

        node = Node(currentUser)

        result = node.filterIns(id=id, label=label, sort=args.get('sort'), skip=args['skip'], limit=args['limit'], filter=args['filter'])
        
        return jsonify(result)

    @app.route("%s/<string:labels>/<int:id>/outs" % baseApiUrl, methods=['GET'])
    @app.route("%s/<string:labels>/<int:id>/outs/<string:label>" % baseApiUrl, methods=['GET'])
    @OptionalSecured
    def fetchOuts(currentUser, labels, id, label=None):
        assertSafe(labels)

        args = dict(
            limit = 10,
            skip = 0,
            filter="true",
            sort=None
        )

        Utils.update(args, request.args)

        node = Node(currentUser)

        result = dao.filterOuts(id = id, label=label, sort=args.get('sort'), skip=args['skip'], limit=args['limit'], filter=args['filter'])
        
        return jsonify(result)

    @app.route("%s/<string:label>" % baseApiUrl, methods=['POST'])
    @Secured
    def create(currentUser, label):
        assertSafe(label)
        body = request.json

        AppRegister.validate(label, body, method='post')

        node = Node(currentUser, label=label, data=body)

        result = node.save()

        return jsonify(result)

    @app.route("%s/<string:label>/merge" % baseApiUrl, methods=['POST'])
    @Secured
    def merge(currentUser, label):
        assertSafe(label)
        body = request.json

        AppRegister.validate(label, body, method='merge')

        if not body.get('compare') or not body.get('data'):
            abort(400, 'You should provide compare and data attributes for a merge')

        node = Node(currentUser, label=label, compare=compare, data=body)

        result = node.save()

        return jsonify(result)

    @app.route("%s/<string:label>/<int:id>" % baseApiUrl, methods=['PUT'])
    @Secured
    def update(currentUser, label, id):
        assertSafe(label)
        body = request.json

        AppRegister.validate(label, body, method='put')

        node = Node(currentUser, id=id, data=body)

        result = node.save()

        if result.get('id'):
            return jsonify(result)
        
        abort(404, 'Not found or not authorized')

    @app.route("%s/<string:label>/<int:id>/<int:end>/<string:rel>" % baseApiUrl, methods=['PUT'])
    @OptionalSecured
    def relate(currentUser, label, id, end, rel):
        assertSafe(label)

        node = Node(currentUser)

        node.to(label=label, start=id, end=end, rel=rel, cascade=request.args.get('cascade', False), owner=currentUser.get('id'))

        return jsonify({})

    @app.route("%s/<string:label>/<int:id>/<int:end>" % baseApiUrl, methods=['DELETE'])
    @OptionalSecured
    def unrelate(currentUser, label, id, end):
        assertSafe(label)

        node = Node(currentUser)

        result = node.unrelate(label=label, start=id, end=end, owner=currentUser.get('id'))

        if result:
            return jsonify({})
            
        abort(404, 'Not found or not authorized')

    @app.route("%s/<string:label>/<int:id>" % baseApiUrl, methods=['DELETE'])
    @OptionalSecured
    def remove(currentUser, label, id):
        assertSafe(label)

        node = Node(currentUser, id=id)

        result = node.fetch()

        if result.get('id'):
            if 'UPLOAD' in result['labels']:
                result = node.remove_upload(uploadId=result['id'], userId=currentUser.get('id'))
            else:
                result = node.delete()

            if result:
                return jsonify({})

        abort(404, 'Not found or not authorized')

    @app.route("%s/<string:label>/<int:id>/ins/<string:rel>" % baseApiUrl, methods=['DELETE'])
    @Secured
    def removeIns(currentUser, label, id, rel):
        assertSafe(label)

        node = Node(currentUser)
        node.deleteIns(id=id, rel=rel)

        jsonify({})

    @app.route("%s/<string:label>/<int:id>/outs/<string:rel>" % baseApiUrl, methods=['DELETE'])
    @Secured
    def removeOuts(currentUser, label, id, rel):
        assertSafe(label)

        node = Node(currentUser)
        node.deleteOuts(id=id, rel=rel)

        jsonify({})

    @app.route("%s/<string:label>/<int:id>/upload" % baseApiUrl, methods=['GET'])
    @OptionalSecured
    def get_upload(currentUser, label, id):
        assertSafe(label)

        node = Node(currentUser)

        upload = node.fetch_upload(id=id)

        return jsonify(upload.get_payload())

    @app.route("%s/<string:label>/<int:id>/upload" % baseApiUrl, methods=['POST'])
    @Secured
    @Uploader(app)
    def add_upload(currentUser, label, id, meta):
        assertSafe(label)

        node = Node(currentUser)

        upload = node.add_upload(
            userId=currentUser.get('id'),
            originId=id, 
            fileId = meta['fileId'],
            fileUrl = meta['fileUrl'],
            labels = ['UPLOAD'])

        return jsonify(upload.get_payload())