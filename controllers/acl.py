import re
from flask import request, abort, jsonify
from decorators.secured import OptionalSecured, Secured, Authorized
from controllers.result import Result
from exceptions.conflict import ConflictException
from exceptions.unauthorized import UnauthorizedException
from utils.utils import Utils
from daos.acl_dao import AclDao
from decorators.uploader import Uploader
import settings

def Controller(app):
  ADMIN_LEVEL = 1
  baseApiUrl = "/api/v1/acl"
  aclDao = AclDao()

  @app.route("%s/<int:userId>/group/<string:groupCode>" % baseApiUrl, methods=['POST'])
  @Authorized(ADMIN_LEVEL)
  def addUserToGroup(currentUser, userId, groupCode):
    aclDao.addUserGroup(userId=userId, groupCode=groupCode.upper())

    return jsonify({})

  @app.route("%s/<int:userId>/group/<string:groupCode>" % baseApiUrl, methods=['DELETE'])
  @Authorized(ADMIN_LEVEL)
  def removeUserFromGroup(currentUser, userId, groupCode):
    aclDao.removeUserGroup(userId=userId, groupCode=groupCode.upper())

    return jsonify({})

  @app.route(baseApiUrl, methods=["GET"])
  @Authorized(ADMIN_LEVEL)
  def getAcl(currentUser):
    pass

  @app.route("%s/group/<string:groupCode>/<string:acl>" % baseApiUrl, methods=["POST"])
  @Authorized(ADMIN_LEVEL)
  def setAcl(currentUser, groupCode, acl):
    body = request.json