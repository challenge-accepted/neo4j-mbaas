import json
from flask import Response
from models.base import BaseModel
from models.list import BaseList

class Result():
    
    def __init__(self, code=200, error=""):
        self.code = code
        self.error = error
        self.payload = dict()
        
    def add(self, key, value):
        if isinstance(value, BaseModel) or isinstance(value, BaseList):
            value = value.get_payload()

        self.payload[key] = value
        
    def set_payload(self, data=dict()):
        self.payload = data
        
    def json(self):
        data = dict(
            code = self.code,
            error = self.error,
            payload = self.payload
        )
        
        return Response(json.dumps(data), mimetype='application/json')