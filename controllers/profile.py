from flask import request, abort, jsonify
from decorators.secured import Secured
from decorators.uploader import Uploader
from models.user_model import UserModel
from daos.profile_dao import ProfileDao
from exceptions.conflict import ConflictException
from exceptions.unauthorized import UnauthorizedException
import settings

def Controller(app):
    profileDao = ProfileDao()

    @app.route("/api/v1/profile", methods=['GET'])
    @Secured
    def myProfile(currentUser):
        profile = profileDao.getProfileById(userId = currentUser.get('id'))

        # clean up
        currentUser.public_user()
        currentUser.add("profile", profile)

        return jsonify(currentUser.get_payload())

    @app.route("/api/v1/profile/<int:userId>", methods=['GET'])
    def userProfile(userId):
        profile = profileDao.getProfileById(userId=userId)

        return jsonify(profile)

    @app.route("/api/v1/profile", methods=['PUT'])
    @Secured
    def updateProfile(currentUser):
        body = request.json

        if len(body) > 0:
            # clean up
            if 'id' in body:
                del body['id']
            if 'credits' in body:
                del body['credits']
                
            profileDao.updateProfile(userId=currentUser.get('id'), fields=body)

        return jsonify({})

    @app.route("/api/v1/profile/picture", methods=['POST'])
    @Secured
    @Uploader(app)
    def uploadPicture(currentUser, meta):
        uploads = profileDao.list_uploads(originId = currentUser.get('id'))

        for record in uploads.get_payload():
            profileDao.remove_upload(uploadId = record['id'])

        upload = profileDao.add_upload(
            userId=currentUser.get('id'),
            originId=currentUser.get('id'), 
            fileId = meta['fileId'],
            fileUrl = meta['fileUrl'],
            labels = ['PICTURE'])

        return jsonify(upload)

    @app.route("/api/v1/profile/picture/<int:id>", methods=['DELETE'])
    @Secured
    def deletePicture(currentUser, id):
        profileDao.remove_upload(uploadId=id, userId=currentUser.get('id'))

        return jsonify({})