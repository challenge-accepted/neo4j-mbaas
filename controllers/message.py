from copy import copy
from flask import request, abort, jsonify
from decorators.secured import Secured
from controllers.result import Result
from exceptions.conflict import ConflictException
from exceptions.unauthorized import UnauthorizedException
from daos.message_dao import MessageDao
from utils.utils import Utils

def Controller(app):
    messageDao = MessageDao()
    baseApiUrl = "/api/v1/message"

    @app.route(baseApiUrl, methods=['GET'])
    @Secured
    def listMyMessages(currentUser):
        messages = messageDao.fetch(userId=currentUser.get('id'))

        # clear old messages
        messageDao.clear(userId=currentUser.get('id'))

        return jsonify(messages)

    @app.route(baseApiUrl, methods=['POST'])
    @Secured
    def sendMeMessage(currentUser):
        body = request.json

        message = messageDao.create(userId=currentUser.get('id'), message=body['message'])

        return jsonify(message)