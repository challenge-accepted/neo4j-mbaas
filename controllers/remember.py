from flask import render_template
import settings

def Controller(app):

    @app.route("/auth/remember", methods=['GET'])
    def remember():
        return render_template("remember.html", title="%s - Trocar senha" % settings.general['company'])