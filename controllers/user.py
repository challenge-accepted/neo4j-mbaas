from flask import request, abort, jsonify
from decorators.secured import Secured, Authorized
from daos.user_dao import UserDao
from models.user_model import UserModel
from exceptions.conflict import ConflictException
from exceptions.unauthorized import UnauthorizedException
from utils.email import EmailUtils
from utils.utils import Utils
from daos.profile_dao import ProfileDao
from utils.social import SocialUtils
import settings

def Controller(app):

    userDao = UserDao()
    profileDao = ProfileDao()

    @app.route("/api/v1/user/google", methods=['POST'])
    def socialSigninGoogle():        
        body = request.json

        try:
            token = body['token']
            email = body['email']

            id = SocialUtils.validateGoogle(token)

            if not id:
                abort(403, 'Invalid token')

            # signin/up user
            token = UserModel.generateToken()
            user = userDao.socialSignin(email=email, token=token, provider='google', id=id)

            return jsonify(user)
        except KeyError:
            abort(401, 'You need to send a valid token for the social signing provider')

    @app.route("/api/v1/user/facebook", methods=['POST'])
    def socialSigninFacebook():
        pass
            
    @app.route("/api/v1/user/signin", methods=['POST'])
    def signin():
        body = request.json

        # content validation
        if('email' not in body or 'password' not in body):
            abort(400)

        try:
            user = userDao.signInUser(email=body['email'], password=body['password'])
        except UnauthorizedException:
            abort(403)

        # clean up
        user.simple_user()

        # add profile
        profile = profileDao.getProfileById(userId=user.get('id'))
        user.add('profile', profile)

        return jsonify(user.get_payload())

    @app.route("/api/v1/user", methods=['POST'])
    def signup():
        body = request.json

        # content validation
        if 'email' not in body or 'password' not in body:
            abort(400)
        if body['email'] is None or len(body['email'].strip()) == 0 or body['password'] is None or len(body['password'].strip()) == 0:
            abort(400)

        try:
            user = userDao.signUpUser(email=body['email'], password=body['password'])
        except ConflictException:
            abort(409)

        # clean up
        user.simple_user()

        return jsonify(user.get_payload())

    @app.route("/api/v1/user", methods=['PUT'])
    @Secured
    def updatePassword(currentUser):
        body = request.json

       # content validation
        if 'password' not in body or body['password'] is None or len(body['password'].strip()) == 0:
            abort(400)

        password = UserModel.generatePassword(body['password'])
        userDao.updatePassword(userId=currentUser.get('id'), password=password)

        return jsonify({})
        

    @app.route("/api/v1/user/<int:userId>", methods=['GET'])
    def userInfo(userId):
        user = userDao.getUser(userId=userId)
        user.public_user()

        return jsonify(user.get_payload())

    @app.route("/api/v1/user/remember", methods = ['GET'])
    def rememberPassword():
        args = dict()

        Utils.update(args, request.args)

        if 'email' not in args:
            abort(400)

        user = userDao.getUserByEmail(email=args['email'])

        if user.get('id') is None:
            abort(401)

        token = user.get('token')
        subject = "%s - Trocar senha" % settings.general['company']
        body = """
            Ola,
            
            Voce requisitou a troca de sua senha em um de nossos serviços. Para continuar, clique no link abaixo:
            
            %s/auth/remember?token=%s

            OBS: Caso nao tenha feito esta requisicao, basta desconsiderar este email.
            
            Atenciosamente, 
            
            Equipe %s""" % (request.host_url.strip('/'), token, settings.general['company'])
        
        EmailUtils.send_email(args['email'], subject, body)

        return jsonify({})
        