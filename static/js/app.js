var webTools = WebTools();

/**
 * Vue Mixin for user operations
 */
var userMixin = {
    data: {
    },
    created: function() {
    },
    methods: {
        getUserId: function() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                    s4() + '-' + s4() + s4() + s4();
        }
    }   
};

/**
 * Vue Mixin for general operations
 */
var appMixin = {
    updated: function() {
        $('.tooltipped').tooltip({delay: 50});
    },
    methods: {
        showMessage: function(message) {
            var toastContent = $('<span><i class="material-icons left">info_outline</i>' + message + "</span>");
            Materialize.toast(toastContent, 3000, 'toast-message');
        },
        showWarning: function(message) {
            var toastContent = $('<span><i class="material-icons left">info_outline</i>' + message + "</span>");
            Materialize.toast(toastContent, 3000, 'toast-warning');
        },
        showError: function(message) {
            var toastContent = $('<span><i class="material-icons left">error_outline</i>' + message + "</span>");
            Materialize.toast(toastContent, 3000, 'toast-error');
        },
        lockScreen: function() {
            $('#loading-overlay').show();
        },
        unlockScreen: function() {
            $('#loading-overlay').hide();
        },
        signoff: function() {
            webTools.remove('user');
        },
        errorHandler: function(error, errorString) {
            code = error.status;
            
            switch(code) {
                case 400:
                    this.showError('Há informações obrigatórias não informadas.');
                    break;
                case 401:
                    this.signoff();
                    window.location.href = '/signin';
                    break;
                case 403:
                    this.showError('Seu usuário não tem autorização para realizar esta operação.');
                    break;
                case 409:
                    this.showError('O nome/email informado já está em uso. Escolha outro e tente novamente.');
                    break;
                case 413:
                    this.showError('O conteúdo foi considerado muito grande para ser enviado.');
                    break;
                case 500:
                    this.showError('Ocorreu um erro inesperado. Por favor, tente novamente mais tarde.');
            }

            this.unlockScreen();
        }
    }
};