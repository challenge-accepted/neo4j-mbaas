
$(document).ready(function() {
    sendBlock = $("#sendLinkBlock");
    sendMessage = $("#sendLinkMessage");
    updateBlock = $("#updatePasswordBlock");
    updateMessage = $("#updatePasswordMessage");

    userToken = "";
    sendEmailUrl = "/api/v1/user/remember";
    updateUrl = "/api/v1/user"

    // page block toggler
    token = /token=([\da-zA-Z\-]+)[&]?/.exec(location.href);
    if(token && token[1]) {
        sendBlock.addClass("hide")
        updateBlock.removeClass("hide")

        userToken = token[1]
    }

    // send button click
    $("#sendBtn").click(function(e) {
        e.preventDefault();
        spinButton("#sendBtn"); 

        clearError();

        userEmail = $("#userEmail").val();

        if(! userEmail) {
            $("#userEmail").addClass("error");
            showError("É necessário informar um email válido.");

            return;
        }

        $.ajax({
            method: 'get',
            url: sendEmailUrl,
            data: {
                email: userEmail
            },
            dataType: 'json',
            success: function(data) {
                spinButton("#sendBtn");
                sendBlock.addClass("hide");
                sendMessage.removeClass("hide");
            },
            error: function(data) {
                spinButton("#sendBtn");
                
                if(data.status === 401) {
                    showError("O email informado não está cadastrado.");
                } else {
                    showError("Houve um erro na tentativa de enviar o email. Por favor, tente mais tarde.")
                }
            }
        });
    });

    // update button click
    $("#updateBtn").click(function(e) {
        e.preventDefault();

        spinButton("#updateBtn");

        clearError();

        password = $("#password").val();
        confirmPassword = $("#confirmPassword").val();

        if(password.trim() === "" || password.trim() != confirmPassword.trim()) {
            $("#password").addClass("error");
            $("#confirmPassword").addClass("error");

            showError("As senhas informadas não conferem ou são inválidas.")

            return;
        }

        $.ajax({
            method: 'put',
            url: updateUrl,
            beforeSend: function(request) {
                request.setRequestHeader("x-auth-token", userToken);
            },
            dataType: 'json',
            data: JSON.stringify({password: password}),
            contentType : 'application/json',
            success: function(data) {
                spinButton("#updateBtn");
                updateBlock.addClass("hide");
                updateMessage.removeClass("hide");

            },
            error: function(data) {
                spinButton("#updateBtn");

                showError("Houve um erro na tentativa trocar sua senha. Por favor, tente mais tarde.")
            }
        });
    });
});

function clearError() {
    $("#userEmail").removeClass("error");
    $("#password").removeClass("error");
    $("#confirmPassword").removeClass("error");

    $(".errorContent").html("");
    $("p.error").addClass("hide");
}

function showError(errorMessage) {
    $(".errorContent").html(errorMessage);
    $("p.error").removeClass("hide");
}

btnContent = "";
function spinButton(selector) {
    if(btnContent) {
        $(selector).removeClass("disabled").html(btnContent);
        btnContent = "";
    } else {
        btnContent = $(selector).html();
        $(selector).addClass("disabled").html("<i class='fa fa-spinner fa-spin'></i>");
    }
}