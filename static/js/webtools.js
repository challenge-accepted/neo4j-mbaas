/**
 * WebTools to integrate with backend API
 * 
 * @autor Arlindo Neto
 */
function WebTools() {
    var storage = window.localStorage;
    
    return {
        save: function(key, value) {
            storage.setItem(key, value);
        },
        
        fetch: function(key, defaultValue) {
            return storage.getItem(key) || defaultValue;
        },
        
        remove: function(key) {
            storage.removeItem(key);
        },
        
        saveObject: function(key, object) {
            this.save(key, JSON.stringify(object));
        },
        
        fetchObject: function(key, defaultValue) {
            return JSON.parse( this.fetch(key, defaultValue || "{}") );
        },

        get: function(url, data, token, type) {
            var parameters = "";

            var index = 0;
            for(var key in data) {
                if(index > 0) {
                    parameters += "&";
                }

                parameters += key + "=" + data[key];

                index++;
            }

            return $.ajax({
                type: 'GET',
                dataType: type || 'json',
                url: url,
                data: parameters,
                beforeSend: function (request) {
                    if(token) {
                        request.setRequestHeader("x-user-token", token);
                    }
                }
            });
        },
        
        post: function(url, data, token) {
            return $.ajax({
                type: 'POST',
                dataType: 'json',
                url: url,
                contentType: "application/json",
                data: JSON.stringify(data),
                beforeSend: function (request) {
                    request.setRequestHeader("x-user-token", token);
                }
            });
        },

        put: function(url, data, token) {
            return $.ajax({
                type: 'PUT',
                dataType: 'json',
                url: url,
                contentType: "application/json",
                data: JSON.stringify(data),
                beforeSend: function (request) {
                    request.setRequestHeader("x-user-token", token);
                }
            });
        },

        delete: function(url, token) {
            return $.ajax({
                type: 'DELETE',
                dataType: 'json',
                url: url,
                contentType: "application/json",
                beforeSend: function (request) {
                    request.setRequestHeader("x-user-token", token);
                }
            });
        }
    };
}