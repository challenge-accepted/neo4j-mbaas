/**
 * Main page control functions
 */
var mainView = new Vue({
    el: '#main',
    mixins: [appMixin, userMixin, mapMixin],
    data: {
        adminUrl: '/api/v1/admin',
        usersList: [],
        routesList: [],
        creditList: [],
        audit: {},
        route: {},
        email: '',
        password: '',
        confirm: '',
        passphrase: '',
        amount: 0,
        bonusCredit: false,
        search: '',
        month: new Date().getMonth() + 1,
        year: new Date().getFullYear()
    },
    methods: {
        nope: function(){},
        listUsers: function() {
            var self = this;

            this.lockScreen();            
            webTools.get(this.adminUrl + "/user", {
                skip: 0,
                limit: 15
            }).done(function(data) {
                self.usersList = data.payload.users;
            }).fail(this.errorHandler)
            .always(this.unlockScreen);
        },
        listRoutes: function() {
            var self = this;

            this.lockScreen();            
            webTools.get(this.adminUrl + "/region", {
                skip: 0,
                limit: 15
            }).done(function(data) {
                self.routesList = data.payload.regions;    
            }).fail(this.errorHandler)
            .always(this.unlockScreen);
        },
        listCredits: function(user) {
            var self = this;
            var fromDate = new Date(this.year, this.month - 1, 1).getTime();
            var data = {
                fromDate: fromDate
            };

            if(user) {
                data.userId = user.id
            }

            this.lockScreen();            
            webTools.get(this.adminUrl + "/credit", data)
            .done(function(data) {
                if(user) {
                    self.audit = {
                        user: user,
                        credits: data.payload.credits
                    };
                } else {
                    self.creditList = data.payload.credits;
                }
            }).fail(this.errorHandler)
            .always(this.unlockScreen);
        },
        updateRoute: function() {
            var self = this;

            var start = this.route.start.toString().replace(']', '').replace('[', '').split(",");
            var end = this.route.end.toString().replace(']', '').replace('[', '').split(",");

            this.route.start = [parseFloat(start[0]), parseFloat(start[1])];
            this.route.end = [parseFloat(end[0]), parseFloat(end[1])];
            this.route.limit = parseInt(this.route.limit);

            this.lockScreen();            
            webTools.post(this.adminUrl + "/region", {
                name: this.route.name,
                code: this.route.code,
                limit: this.route.limit,
                start: this.route.start,
                end: this.route.end,
                passphrase: this.passphrase
            })
            .done(function(data) {
                self.showMessage('Rota atualizada com sucesso.');

                self.listRoutes();

                self.passphrase = '';
            }).fail(this.errorHandler)
            .always(this.unlockScreen);
        },
        signup: function() {
            var self = this;

            if(! this.email || ! this.password) {
                this.showError('Todos os campos são obrigatórios.');
                return;
            }

            if(this.password !== this.confirm) {
                this.showError('As duas senhas devem ser iguais.');
                return;
            }

            this.lockScreen();            
            webTools.post(this.adminUrl + "/signup", {
                email: this.email,
                password: this.password,
                passphrase: this.passphrase
            }).done(function(data) {
                self.showMessage('Novo usuário criado com sucesso.');

                self.email = "";
                self.password = "";
                self.passphrase = "";
            }).fail(this.errorHandler)
            .always(this.unlockScreen);
        },
        addCredit: function() {
            var self = this;

            if(! this.email || ! this.amount) {
                this.showError('Todos os campos são obrigatórios.');
                return;
            }
            
            this.lockScreen();            
            webTools.put(this.adminUrl + "/credit", {
                email: this.email,
                credits: parseInt(this.amount),
                bonus: this.bonusCredit,
                passphrase: this.passphrase
            }).done(function(data) {
                self.showMessage('Total de créditos do motorista: ' + data.payload.credits);

                self.usersList.map(function(elem) {
                    if(elem.email === self.email) {
                        elem.profile.credits = data.payload.credits;
                    }
                });

                self.bonusCredit = false;
                self.amount = 0;
                self.passphrase = "";
            }).fail(this.errorHandler)
            .always(this.unlockScreen);
        },
        format: function(date) {
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();

            return day + '/' + month + '/' + year;
        }
    },
    computed: {
        users: function() {
            var self = this;

            if(this.search.trim() === '') {
                return this.usersList;
            } else {
                return this.usersList.filter(function(elem, i, array) {
                    var pattern = new RegExp(".*" + self.search + ".*", "i");

                    return elem.email.match(pattern);
                });
            }
        },
        routes: function() {
            var self = this;

            if(this.search.trim() === '') {
                return this.routesList;
            } else {
                return this.routesList.filter(function(elem, i, array) {
                    var pattern = new RegExp(".*" + self.search + ".*", "i");

                    return elem.name.match(pattern) || elem.code.match(pattern);
                });
            }
        }
    }
});