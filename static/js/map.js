var mapMixin = {
    data: {
        apiUrl: '/api/v1/route',
        region: 'GC',
        map: null,
        mapInit: false,
        markers: [],
        enablePassenger: false
    },
    created: function() {
        var self = this;

        $(document).on('map-loaded', function() {
            self.map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                scrollwheel: false,
                zoom: 14
            });

            self.map.addListener('click', function(e) {
                var id = self.getUserId();
                
                if(self.enablePassenger) {
                    webTools.post(self.apiUrl + "/" + id, {
                        region: self.region,
                        lat: e.latLng.lat(),
                        lng: e.latLng.lng(),
                        temp: true
                    })
                    .done(function() {
                        var marker = new google.maps.Marker({
                            position: e.latLng,
                            customInfo: id,
                            icon: '/static/images/passenger-temp.png',
                            map: self.map
                        });
                        
                        google.maps.event.addListener(marker, 'click', function() {
                            webTools.post(self.apiUrl + "/" + marker.customInfo, {
                                region: self.region
                            })
                            .done(function() {
                                marker.setMap(null);
                            });
                        });
                    });
                }
            });

            self.getData()
        });
    },
    methods: {
        getData: function() {
            var self = this;

            webTools.get(this.apiUrl, {
                region: this.region
            })
            .done(function(data) {
                var meta = data.payload.meta;

                // CLEAN UP
                for(marker in self.markers) {
                    self.markers[marker].setMap(null);
                }

                self.markers = [];

                if(! self.mapInit) {
                    self.boundMap(meta.region.start, meta.region.end);
                }

                // PASSENGERS

                for(var index in meta.passengers) {
                    if(! meta.passengers[index].temp) {
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(meta.passengers[index].lat, meta.passengers[index].lng),
                            icon: '/static/images/passenger.png',
                            map: self.map
                        });

                        self.markers.push(marker);
                    }
                }

                // DRIVERS

                for(var index in meta.drivers) {
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(meta.drivers[index].lat, meta.drivers[index].lng),
                        icon: '/static/images/car.png',
                        map: self.map
                    });

                    self.markers.push(marker);
                }

                setTimeout(function() {
                    self.getData();
                }, 20000);
            })
            .fail(webTools.errorHandler)
            .always(function() {

            });
        },

        boundMap: function(region_start, region_end) {
            // BOUNDS
            var start = new google.maps.LatLng(region_start[0], region_start[1]);
            var end = new google.maps.LatLng(region_end[0], region_end[1]);

            var bounds = new google.maps.LatLngBounds();

            bounds.extend(start);
            bounds.extend(end);

            this.map.fitBounds(bounds);

            this.mapInit = true;
        }
    }
};