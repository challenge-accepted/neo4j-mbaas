import graphene
from graph.schemas.user import User

class Query(graphene.ObjectType):
    user = graphene.Field(User, id=graphene.Int())

    def resolve_user(self, args, request, info):
        print(args)
        return User(**dict(name="Arlindo Neto"))

schema = graphene.Schema(query=Query)