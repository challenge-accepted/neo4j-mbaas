from flask_graphql import GraphQLView

# register schemas
from graph.query import Query

class GraphQl():
    
    def __init__(self, app):
        app.add_url_rule('/graphql', view_func=GraphQLView.as_view('graphql', schema=Query, graphiql=True))