Feature: user api test

Background:
	* url 'http://localserver:9999'
	Given path '/api/debug/db'
	When method delete
	Then status 200

Scenario: Create a user and pick the token
	* def user =
	"""
	{
	  "email": "arlindo@gmail.com",
	  "password": "super123"
	}
	"""
	Given path '/api/v1/user'
	And request user
	When method post
	Then status 200
	And assert response.token != null

	* def token = response.token
	* print token

Scenario: Email already in use
	* def user =
	"""
	{
	  "email": "arlindo@gmail.com",
	  "password": "super123"
	}
	"""

	Given path '/api/v1/user'
	And request user
	When method post
	Then status 200

	Given path '/api/v1/user'
	And request user
	When method post
	Then status 409

Scenario: Invalid request creating user
	* def user =
	"""
	{
	  "email": "",
	  "password": ""
	}
	"""
	Given path '/api/v1/user'
	And request user
	When method post
	Then status 400

Scenario: Signing in with password successfully updated
	* def user =
	"""
	{
	  "email": "arlindo@gmail.com",
	  "password": "super123"
	}
	"""
	Given path '/api/v1/user'
	And request user
	When method post
	Then status 200
	* header Authorization = response.token

	* def user =
	"""
	{
	  "password": "super"
	}
	"""
	Given path '/api/v1/user'
	And request user
	When method put
	Then status 200

	* def validCredentials = 
	"""
	{
	  "email": "arlindo@gmail.com",
	  "password": "super"
	}
	"""

	Given path '/api/v1/user/signin'
	And request validCredentials
	When method post
	Then status 200

Scenario: Invalid request updating password
	* def user =
	"""
	{
	  "email": "arlindo@gmail.com",
	  "password": "super123"
	}
	"""
	Given path '/api/v1/user'
	And request user
	When method post
	Then status 200

	* header Authorization = response.token
	
	* def user =
	"""
	{
	  "password": ""
	}
	"""
	Given path '/api/v1/user'
	And request user
	When method put
	Then status 400

Scenario: Fail to update password due to lack of token
	* def user =
	"""
	{
	  "email": "arlindo@gmail.com",
	  "password": "super123"
	}
	"""
	Given path '/api/v1/user'
	And request user
	When method post
	Then status 200
	
	* def user =
	"""
	{
	  "password": "super"
	}
	"""
	Given path '/api/v1/user'
	And request user
	When method put
	Then status 401

Scenario: Fail to sign in due to invalid password
	* def user =
	"""
	{
	  "email": "arlindo@gmail.com",
	  "password": "super123"
	}
	"""
	Given path '/api/v1/user'
	And request user
	When method post
	Then status 200
	* header Authorization = response.token

	* def invalidCredentials = 
	"""
	{
	  "email": "arlindo@gmail.com",
	  "password": "super"
	}
	"""

	Given path '/api/v1/user/signin'
	And request invalidCredentials
	When method post
	Then status 403

Scenario: Fail to sign in due to invalid email
	* def user =
	"""
	{
	  "email": "arlindo@gmail.com",
	  "password": "super123"
	}
	"""
	Given path '/api/v1/user'
	And request user
	When method post
	Then status 200
	* header Authorization = response.token

	* def invalidCredentials = 
	"""
	{
	  "email": "invalid.email@gmail.com",
	  "password": "super"
	}
	"""

	Given path '/api/v1/user/signin'
	And request invalidCredentials
	When method post
	Then status 403

Scenario: Fail to sign in due to invalid request
	* def user =
	"""
	{
	  "email": "arlindo@gmail.com",
	  "password": "super123"
	}
	"""
	Given path '/api/v1/user'
	And request user
	When method post
	Then status 200
	* header Authorization = response.token

	* def invalidCredentials = 
	"""
	{
	  "email": "invalid.email@gmail.com",
	  "pass": "super123"
	}
	"""

	Given path '/api/v1/user/signin'
	And request invalidCredentials
	When method post
	Then status 400

















