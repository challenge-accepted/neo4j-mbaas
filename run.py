import os
import settings
from inspect import isfunction, getmembers
from flask import Flask, jsonify
from flask_cors import CORS, cross_origin
from exceptions.payment_required import PaymentRequiredException
from db.neo4j import Neo4j
from db.query import clearAll
from lib.register import AppRegister

#from controllers.data import DataController

app = Flask(__name__)

# enable CORS
CORS(app)

# Upload Service Settings
UPLOAD_FOLDER = os.path.join(os.getcwd(), "uploads/")
MAX_CONTENT_LENGTH = 5 * 1024 * 1024
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = MAX_CONTENT_LENGTH

# register app setup to AppRegistry static/global utility
AppRegister.app = app

basePath = os.path.dirname(os.path.abspath(__file__))

# load custom services (can override default services)
for (dirpath, dirnames, filenames) in os.walk("%s/services" % basePath):
    for filename in filenames:
        try:
            __import__("services.%s" % filename[:-3], globals(), locals())
        except AttributeError:
            pass
    break

# load controllers
for (dirpath, dirnames, filenames) in os.walk("%s/controllers" % basePath):
    for filename in filenames:
        try:
            controller = __import__("controllers.%s" % filename[:-3], globals(), locals(), ['Controller'], 0)
            
            AppRegister.registerController(controller.Controller)
        except AttributeError:
            pass
    break

# Initialize Database
Neo4j.initialize(**settings.database)

# Custom Exceptions
# Translates a Precondition Fail to a Payment Required
@app.errorhandler(PaymentRequiredException)
def payment_required(error):
    return "Payment Required", 402

# Test endpoint to clear the database
if settings.env == 'test':
    @app.route("/api/debug/db", methods=['DELETE'])
    def clearDatabase():
        clearAll()

        return jsonify({})

if __name__ == "__main__":
    app.run(
        host=settings.server['host'], 
        port=settings.server['port'],
        debug=settings.server['debug'] 
    )
