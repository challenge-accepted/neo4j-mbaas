from flask import request, abort
from daos.data_dao import DataDao
from db.query import Transactional

class Node(DataDao):

    def __init__(self, owner, label='', compare={}, data={}, id=None):
        self.owner = owner
        self.label = label
        self.compare = compare
        self.data = data
        self.id = id

    @Transactional
    def fetch(self, tx):
        if not self.id:
            abort(400, 'You need to specify the entity\'s id')

        self.data = self.get(tx=tx, id=self.id)

        return self.data

    @Transactional
    def save(self, tx, data=None):
        if not data:
            data = self.data

        if self.id:
            return self.update(tx=tx, id=self.id, data=data, owner=self.owner.get('id'))
        elif self.compare:
            return self.merge(tx=tx, label=self.label.upper(), compare=self.compare, data=data, owner=self.owner.get('id'))
        else:
            return self.create(tx=tx, label=self.label.upper(), data=data, owner=self.owner.get('id'))

    @Transactional
    def delete(self, tx):
        if not self.id:
            abort(400, 'You need to specify the entity\'s id')

        return super().delete(tx=tx, id=self.id, owner=self.owner.get('id'))