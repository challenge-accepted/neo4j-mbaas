from datetime import datetime
from db.query import Query, Transactional, doQuery
from exceptions.conflict import ConflictException
from exceptions.unauthorized import UnauthorizedException
from utils.dao import DaoUtils
from utils.utils import Utils
import settings

class AclDao():

  @Query("""
      MATCH (user:USER)
      WHERE id(user) = {userId}
      MERGE (user)-[:JOINED]->(group:GROUP {code: {groupCode}})
      ON CREATE SET group.acl = {acl}
      """)
  def addUserGroup(self, result, userId, groupCode, acl=settings.server.defaultAcl):
      pass

  @Query("""
      MATCH (user:USER)-[joined:JOINED]->(group:GROUP {code: {groupCode}})
      WHERE id(user) = {userId}
      DELETE joined
      """)
  def removeUserGroup(self, result, userId, groupCode):
      pass

  @Query("""
      MATCH (group:GROUP {code: {groupCode}})
      SET group.acl = {acl}
      """)
  def addAcl(self, result, groupCode, acl):
    pass