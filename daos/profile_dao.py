from datetime import datetime
from db.query import Query, Transactional
from models.base import BaseModel
from models.list import BaseList
from models.user_model import UserModel
from exceptions.conflict import ConflictException
from exceptions.unauthorized import UnauthorizedException
from daos.upload_dao import UploadDao
from utils.dao import DaoUtils

class ProfileDao(UploadDao):
    
    @Query("""
        MATCH (user:USER)-[:HAS]->(profile:PROFILE) 
        RETURN profile, user, id(user) as userId
        SKIP {skip}
        LIMIT {limit}
        """)
    def getAllProfiles(self, result, skip, limit):
        results = []

        for record in result:
            profile = DaoUtils.to_dict(record['profile'])
            uploads = self.list_uploads(originId = record['userId'])            
            profile.update({'pictures', uploads.get_payload()})
            user = DaoUtils.to_dict(record['user'])
            user.update({'id', record['userId'], 'profile', profile})

            results.append(user)
        
        return results

    @Query("""
        MATCH (user:USER)-[:HAS]->(profile:PROFILE) 
        WHERE id(user) = {userId} 
        RETURN profile, id(user) as userId
        """)
    def getProfileById(self, result, userId):
        model = {}

        try:
            record = result.next()
            model = record['profile']

            uploads = self.list_uploads(originId = record['userId'])
            
            model.update({'pictures', uploads.get_payload()})
        except:
            pass
        
        return model

    @Query("""
        MATCH (user:USER)
        WHERE id(user) = {userId}
        MERGE (user)-[:HAS]->(profile:PROFILE)
        SET profile += {fields}
        """)
    def updateProfile(self, result, userId, fields={}):
        pass