
from db.query import Query, Transactional
from models.base import BaseModel
from models.list import BaseList
from exceptions.conflict import ConflictException
from exceptions.unauthorized import UnauthorizedException
from werkzeug.exceptions import NotFound
from utils.file.file import FileUtils
from utils.dao import DaoUtils
import settings

class UploadDao():

    @Query("""
        MATCH (entity)-[:HAS]->(u:UPLOAD)
        WHERE id(u) = {id}
        RETURN 
            u.name as name,
            u.description as description,
            u.file_url as file_url, 
            id(u) as id,
            labels(u) as labels
        """)
    def fetch_upload(self, result, id):
        uploads = BaseList()

        try:
            record = result.next()

            upload = BaseModel()
            upload.add('name', record['name'])
            upload.add('description', record['description'])
            upload.add('file_url', record['file_url'])
            upload.add('id', record['id'])
            upload.add('labels', [])

            # process labels
            labels = record['labels']
            for label in labels:
                if label != 'UPLOAD':
                    upload.get('labels').append(label)

            return upload

        except Exception as ex:
            print(ex)

            raise ex

    @Query("""
        MATCH (entity)-[:HAS]->(u:UPLOAD)
        WHERE id(entity) = {originId}
        RETURN 
            u.name as name,
            u.description as description,
            u.file_url as file_url, 
            id(u) as id,
            labels(u) as labels
        ORDER BY u.date_create DESC
        """)
    def list_uploads(self, result, originId):
        uploads = BaseList()

        for record in result:
            upload = BaseModel()
            upload.add('name', record['name'])
            upload.add('description', record['description'])
            upload.add('file_url', record['file_url'])
            upload.add('id', record['id'])
            upload.add('labels', [])

            # process labels
            labels = record['labels']
            for label in labels:
                if label != 'UPLOAD':
                    upload.get('labels').append(label)

            uploads.add(upload)

        return uploads

    @Transactional
    def add_upload(self, tx, userId, originId, fileId, fileUrl, name = "", description = "", labels = []):
        model = BaseModel()

        record = self.create_upload(tx = tx, userId=userId, originId=originId, name=name, description=description,
            fileId=fileId, fileUrl=fileUrl, strategy=settings.server['fileStrategy'])
        
        if record['id'] > 0:
            for label in labels:
                statement = """
                    MATCH (u:UPLOAD)
                    WHERE id(u) = {uploadId}
                    SET u :%s
                    """ % label

                tx.run(statement, parameters = dict(uploadId = record['id']))

            model.add_from_record(record['upload'])
            model.add('id', record['id'])
            model.add('labels', labels)

            return model

        raise NotFound()

    @Transactional
    def remove_upload(self, tx, uploadId, userId = None):
        drive = FileUtils.getDrive()

        if userId is not None:
            fileMeta = self.remove_upload_from_db_from_owner(tx=tx, id=uploadId, userId=userId)
        else:
            fileMeta = self.remove_upload_from_db(tx=tx, id=uploadId)
    
        try:
            drive.delete(fileMeta['file_id'])

            return True
        except KeyError as ex:
            pass
        except Exception as ex:
            print(ex)

            raise ex

    @Transactional
    def remove_orphan_uploads(self, tx):
        drive = FileUtils.getDrive()

        orphans = self.find_orphans(tx=tx)

        for orphan in orphans:
            fileMeta = self.remove_upload_from_db(tx=tx, id=orphan['id'])
        
            try:
                drive.delete(fileMeta['file_id'])
            except Exception as ex:
                print(ex)

                raise ex

    @Query("""
        MATCH (u:UPLOAD) 
        WHERE SIZE (()-[:HAS]->(u)) = 0 
        RETURN id(u) as id
        """)
    def find_orphans(self, result):
        return result

    @Query("""
        MATCH (entity)
        WHERE id(entity) = {originId}
        MATCH (user:USER)
        WHERE id(user) = {userId}
        CREATE (entity)-[:HAS]->(u:UPLOAD)<-[:UPLOADED]-(user)
        SET u.name = {name},
            u.description = {description},
            u.file_id = {fileId}, 
            u.file_url = {fileUrl}, 
            u.date_create = timestamp(),
            u.upload_strategy = {strategy}
        RETURN u as upload, id(u) as id
        """)
    def create_upload(self, result, originId, userId, name, description, fileId, fileUrl, strategy):
        try:
            record = result.next()

            return record
        except Exception as ex:
            print(ex);
            return dict(
                id = 0
            )

    @Query("""
        MATCH (u:UPLOAD)
        WHERE id(u) = {id}
        WITH u, u.file_id as file_id
        DETACH DELETE u
        RETURN file_id
        """)
    def remove_upload_from_db(self, result, id):
        try:
            record = result.next()

            return record
        except Exception as ex:
            print(ex);
            return dict()

    @Query("""
        MATCH (user:USER)-[:UPLOADED]->(u:UPLOAD)
        WHERE id(u) = {id}
            and id(user) = {userId}
        WITH u, u.file_id as file_id
        DETACH DELETE u
        RETURN file_id
        """)
    def remove_upload_from_db_from_owner(self, result, id, userId):
        try:
            record = result.next()

            return record
        except Exception as ex:
            print(ex);
            return dict()