from .base_filter_dao import BaseFilterDao
from db.query import Query, doQuery, Transactional

class FilterDao(BaseFilterDao):

	@Query("""
		MATCH (entity)
		WHERE id(entity) = {id}
		RETURN entity, id(entity) as id, labels(entity) as labels
		""")
	def get(self, result, id):
		results = self.process(result)

		if len(results) > 0:
			return results[0]

		return {}


	@Transactional
	def filter(self, tx, labels, filter="true", user=None, sort=None, skip=0, limit=10):
		result = doQuery("""
			MATCH (entity)
			WHERE labels(entity) in {labels}
			AND (%s)
			RETURN entity, id(entity) as id, labels(entity) as labels
			ORDER BY %s
			SKIP {skip}
			LIMIT {limit}
			""" % (filter, (sort if sort else 'entity.date_create DESC')), user=user, labels=labels, skip=skip, limit=limit)

		return self.process(result)

	@Transactional
	def filterIns(self, tx, id, filter="true", sort=None, skip=0, limit=10, label=None):
		result = doQuery("""
			MATCH (entity)-->(dest)
			WHERE id(dest) = {id}
			AND NOT ANY(label IN LABELS(entity) WHERE label IN ['USER', 'PROFILE', 'GROUP'])
			AND (%s)
			%s
			RETURN entity, id(entity) as id, labels(entity) as labels
			ORDER BY %s
			SKIP {skip}
			LIMIT {limit}
			""" % (filter, 
				("AND ANY(label IN LABELS(entity) WHERE label IN ['%s'])" % label.upper() if label is not None else ''),
				(sort if sort else 'entity.date_create DESC')), id=id, skip=skip, limit=limit)

		return self.process(result)

	@Transactional
	def filterOuts(self, tx, id, filter="true", sort=None, skip=0, limit=10, label=None):
		result = doQuery("""
			MATCH (source)-->(entity)
			WHERE id(source) = {id}
			AND NOT ANY(label IN LABELS(entity) WHERE label IN ['USER', 'PROFILE'])
			AND (%s)
			%s
			RETURN entity, id(entity) as id, labels(entity) as labels
			ORDER BY %s
			SKIP {skip}
			LIMIT {limit}
			""" % (filter, 
				("AND ANY(label IN LABELS(entity) WHERE label IN ['%s'])" % label.upper() if label is not None else ''),
				(sort if sort else 'entity.date_create DESC')), id=id, skip=skip, limit=limit)

		return self.process(result)