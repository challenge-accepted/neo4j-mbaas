from db.query import Query, doQuery, Transactional

class RelateDao:

	@Transactional
	def relate(self, tx, label, start, end, rel, cascade=False, owner=None):
		doQuery("""
			MATCH(start)
			WHERE id(start) = {start}
			MATCH(end)
			WHERE id(end) = {end}
			MERGE(start)-[rel:%s]->(end)            
			ON CREATE SET rel.date_create = {now}, rel.creator = {owner}
			SET rel.cascade = {cascade}
			RETURN id(rel) as id
			""" % rel.upper(), tx=tx, start=start, end=end, now=Utils.now(), cascade=cascade, owner=owner)

	@Query("""
		MATCH (start)-[rel]->(end)
		WHERE id(start) = {start}
			AND id(end) = {end}
			AND (rel.creator = {owner} OR rel.creator IS NULL)
		WITH rel, id(rel) as id
		DELETE rel
		RETURN id
		""")
	def unrelate(self, result, label, start, end, owner):
		try:
			result.next()
			return True
		except:
			return False

	@Query("""
		MATCH (entity)-->(dest)
		WHERE id(dest) = {id}
			AND NOT ANY(label IN LABELS(entity) WHERE label IN ['USER', 'PROFILE', 'UPLOAD'])
			AND ANY(label IN LABELS(entity) WHERE label IN [{rel}])
		DETACH DELETE entity
		""")
	def deleteIns(self, result, id, rel=None):
		pass

	@Query("""
		MATCH (dest)-->(entity)
		WHERE id(dest) = {id}
			AND NOT ANY(label IN LABELS(entity) WHERE label IN ['USER', 'PROFILE', 'UPLOAD'])
			AND NOT ANY(label IN LABELS(entity) WHERE label IN [{rel}])
		DETACH DELETE entity
		""")
	def deleteOuts(self, result, id, rel=None):
		pass