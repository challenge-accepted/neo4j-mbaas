from utils.dao import DaoUtils
from db.query import Query
from decorators.private import Private

class BaseFilterDao:
		
	@Private
	def process(self, result):
		results = []

		for record in result:
			node = DaoUtils.to_dict(record['entity'])
			node.update({"id": record['id'], "labels": record['labels']})
			node.update({"outs": self.out_relations(id=record['id'])})
			node.update({"ins": self.in_relations(id=record['id'])})

			results.append(node)

		return results

	@Private
	@Query("""
		OPTIONAL MATCH (entity)-[rel]->(out)
		WHERE id(entity) = {id}
		RETURN id(rel) as id, type(rel) as type, id(out) as to, labels(out) as labels
		ORDER BY rel.date_create DESC
		LIMIT 20
		""")
	def out_relations(self, result, id):
		results = []

		for record in result:
			if record['id']:
				results.append({"id": record['id'], "to": record['to'], "type": record['type'], "labels": record['labels']})
		
		return results

	@Private
	@Query("""
		OPTIONAL MATCH (entity)<-[rel]-(in)
		WHERE id(entity) = {id}
				AND id(in) <> {id}
		RETURN id(rel) as id, type(rel) as type, id(in) as from, labels(in) as labels
		ORDER BY rel.date_create DESC
		LIMIT 20
		""")
	def in_relations(self, result, id):
		results = []

		for record in result:
			if record['id']:
				results.append({"id": record['id'], "from": record['from'], "type": record['type'], "labels": record['labels']})
		
		return results