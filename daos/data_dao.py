from datetime import datetime
from py2neo import Node
from db.query import Query, Transactional, doQuery
from exceptions.conflict import ConflictException
from exceptions.unauthorized import UnauthorizedException
from decorators.private import Private
from daos.upload_dao import UploadDao
from daos.dao_modules.filter_dao import FilterDao
from daos.dao_modules.relate_dao import RelateDao
from utils.dao import DaoUtils
from utils.utils import Utils

class DataDao(UploadDao, FilterDao, RelateDao):
    
    def run(self, query, parameters):
        results = []
        result = doQuery(query, **parameters)

        for record in result:
            results.append(record)

        return results

    @Transactional
    def create(self, tx, label, data, owner):
        embed = Utils.create_body(data)

        result = doQuery("""
            MATCH (user:USER) 
            WHERE id(user) = {owner}
            CREATE (user)-[:CREATED]->(entity:%s)
            SET entity.creator = {owner}, entity += {data}
            WITH entity
            RETURN entity, id(entity) as id
            """ % label, tx=tx, data=data, owner=owner)

        results = {}

        try:
            record = result.next()
            record['entity'].update({"id": record['id']})

            results = record['entity']
        except:
            pass
        
        return results

    @Transactional
    def merge(self, tx, label, compare, data, owner):
        embed = Utils.create_body(data)

        result = doQuery("""
            MATCH (user:USER) 
            WHERE id(user) = {owner}
            MERGE (user)-[:CREATED]->(entity:%s {compare})
            ON CREATE SET entity.creator = {owner}
            SET entity += {data}
            WITH entity
            RETURN entity, id(entity) as id
            """ % label, tx=tx, compare=compare, data=data, owner=owner)

        results = {}

        try:
            record = result.next()
            record['entity'].update({"id": record['id']})

            results = record['entity']
        except:
            pass
        
        return results

    @Transactional
    def update(self, tx, id, data, owner):
        embed = Utils.update_body(data)

        result = doQuery("""
            MATCH (entity)
            WHERE id(entity) = {id}
                AND entity.creator = {owner}
            SET entity += {data}
            RETURN entity, id(entity) as id
            """, tx=tx, id=id, owner=owner, data=data)

        results = {}

        try:
            record = result.next()
            record['entity'].update({"id": record['id']})

            results = record['entity']
        except:
            pass
        
        return results

    @Transactional
    def delete(self, tx, id, owner):
        result = self.delete_cascade(tx=tx, id=id, owner=owner)

        if result:
            self.remove_orphan_uploads(tx=tx)

        return result

    @Private
    @Query("""
        MATCH (entity)
            WHERE id(entity) = {id}
            AND (entity.creator = {owner} OR entity.creator IS NULL)
        OPTIONAL MATCH (entity)-[* {cascade: true}]->(child)
        WITH child, entity, id(entity) as id
        DETACH DELETE entity, child
        RETURN id
        """)
    def delete_cascade(self, result, id, owner):
        try:
            result.next()

            return True
        except:
            return False