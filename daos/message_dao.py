from db.query import Query, Transactional
from models.base import BaseModel
from models.list import BaseList
from utils.dao import DaoUtils

class MessageDao():

    @Query("""
        MATCH (me:USER)--(:INBOX)--(message:MESSAGE)
        WHERE id(me) = {userId}
            and message.read = false
        SET message.read = true
        RETURN message
        ORDER BY message.date DESC
    """)
    def fetch(self, result, userId):
        messages = []

        for record in result:
            messages.append( DaoUtils.to_dict(record['message']) )

        return messages

    @Query("""
        MATCH (me:USER)
        WHERE id(me) = {userId}
        MERGE (me)-[:HAS]->(inbox:INBOX)
        CREATE (inbox)-[:RECEIVED]->(message:MESSAGE)
        SET message.date = timestamp(),
            message.payload = {message},
            message.read = false
        RETURN message
    """)
    def create(self, result, userId, message):
        record = result.next()

        return DaoUtils.to_dict(record['message'])

    @Query("""
        MATCH (me:USER)--(:INBOX)--(message:MESSAGE)
        WHERE id(me) = {userId}
            and message.read = true
        DETACH DELETE message
    """)
    def clear(self, result, userId):
        pass
