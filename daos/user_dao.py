from datetime import datetime
from db.query import Query, Transactional
from models.user_model import UserModel
from models.list import BaseList
from exceptions.conflict import ConflictException
from exceptions.unauthorized import UnauthorizedException

class UserDao():

    @Query("""
        MATCH (user:USER {token: {token}})
        RETURN user, id(user) as id
        """)
    def getUserByToken(self, result, token):
        model = UserModel()

        for record in result:
            model.add_from_record(record['user'])
            model.add("id", record['id'])
            break
        
        return model

    @Query("""
        MATCH (user:USER) WHERE id(user) = {userId}
        SET user.token = {token}
        """)
    def updateUsersToken(self, result, userId, token):
        pass

    @Query("""
        MATCH (user:USER) WHERE id(user) = {userId}
        SET user.password = {password}
        """)
    def updatePassword(self, result, userId, password):
        pass

    @Query("""
        MATCH (user:USER) WHERE id(user) = {userId}
        SET user.active = {active}
        """)
    def udpateActivity(self, result, userId, active):
        pass

    @Query("""
        MATCH (user:USER)
        WHERE LOWER(user.email) = LOWER({email})
        RETURN user, id(user) as id
        """)
    def getUserByEmail(self, result, email):
        model = UserModel()

        for record in result:
            model.add_from_record(record['user'])
            model.add("id", record['id'])
            break
        
        return model

    @Query("""
        CREATE (user:USER) 
        SET user.email      = LOWER({email}), 
            user.password   = {password}, 
            user.token      = {token},
            user.date       = timestamp(),
            user.active     = {active}
        RETURN user, id(user) as id
        """)
    def createUser(self, result, email, password, token, active):
        model = UserModel()

        for record in result:
            model.add_from_record(record['user'])
            model.add("id", record['id'])
            break
        
        return model

    @Query("""
        MERGE (user:USER {provider: {provider}, social_id: {id}}) 
        ON CREATE SET user.email      = LOWER({email}), 
                      user.token      = {token}
        ON UPDATE SET user.token      = {token}
        RETURN user, id(user) as id
        """)
    def socialSignin(self, result, email, token, provider, id):
        model = UserModel()

        for record in result:
            model.add_from_record(record['user'])
            model.add("id", record['id'])
            break
        
        return model

    @Query("""
        MATCH (user:USER) WHERE id(user)={userId}
        RETURN user, id(user) as id
        """)
    def getUser(self, result, userId):
        model = UserModel()

        for record in result:
            model.add_from_record(record['user'])
            model.add("id", record['id'])
        
        return model

    @Transactional
    def signUpUser(self, tx, email, password):
        password = UserModel.generatePassword(password)

        user = self.getUserByEmail(tx=tx, email = email)
    
        # email found
        if user.get('id') is not None:
            raise ConflictException()

        token = user.generateToken()

        user = self.createUser(tx=tx, email=email, password=password, token=token, active=True)

        return user
        
    @Transactional
    def signInUser(self, tx, email, password):
        email = email
        password = UserModel.generatePassword(password)

        user = self.getUserByEmail(tx=tx, email=email)
    
        # email not found or password doesn't match
        if user.get('id') is None or user.get('password') != password:
            raise UnauthorizedException()

        token = user.generateToken()
        #self.updateUsersToken(tx=tx, userId=user.get('id'), token=token)

        user.add("token", token)

        return user