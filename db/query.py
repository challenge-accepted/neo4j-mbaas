from db.neo4j import Neo4j
from inspect import ismethod

# decorator to inject transaction into a class method
def Transactional(method):
    def openTransaction(self=None, tx=None, close=None, *args, **kwargs):
        if tx is not None and close is None: 
            transaction = tx
            close = False
        else:
            transaction = Neo4j.graph().begin()
            close = True

        result = None

        try:
            if self:
                result = method(self=self, tx = transaction, *args, **kwargs)
            else: 
                result = method(tx = transaction, *args, **kwargs)
            
            if not transaction.finished() and close:
                transaction.commit()
        except Exception as ex:
            if close:         
                transaction.rollback()
            
            raise ex

        return result

    return openTransaction 

# decorator to run query over a given or injected transaction
def Query(statement, asTable=False):
    def neo4jQuery(method):
        def startTxAndQuery(self=None, tx=None, parameters=None, *args, **kwargs):
            if tx is None:
                tx = Neo4j.graph()
                        
            if parameters is None and len(kwargs) > 0:
                parameters = kwargs

            result = tx.run(statement, parameters=parameters)

            if asTable:
                result = result.data()

            if self:
                return method(self=self, result=result, **kwargs)
            else:
                return method(result=result, **kwargs)

        return startTxAndQuery

    return neo4jQuery

# function to handle queries from methods body (when custom preparation is needed)
def doQuery(statement, tx=None, *args, **kwargs):
    parameters = {}
    
    if tx is None:
        tx = Neo4j.graph()
                        
    if len(kwargs) > 0:
        parameters = kwargs

    return tx.run(statement, parameters=parameters)

@Query("""
    MATCH (a) DETACH DELETE a
    """)
def clearAll(result):
    """ Only use it in test tear down steps. No confirmations at all, just deletes all nodes/relationships """