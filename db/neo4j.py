from py2neo import Graph, watch

#watch("neo4j.http", level='DEBUG')

class Neo4j():
    connection = None
    settings = None

    @staticmethod
    def initialize(host, port, username, password):
        Neo4j.settings = dict(
            host = host,
            port = port,
            username = username,
            password = password
        )

    @staticmethod
    def graph(): 
        if(Neo4j.settings is None):
            raise Exception("You must initialize database service!")

        if Neo4j.connection is None:
            Neo4j.connection = Graph(
                host=Neo4j.settings['host'], 
                http_port=Neo4j.settings['port'],
                user=Neo4j.settings['username'], 
                password=Neo4j.settings['password'],
                bolt=False
            )

        return Neo4j.connection