# NEO4J BAAS

## Api Documentation

You can check the RAML documentation at [this location](http://localhost:9999/static/developer/api.html)!

## Running

### Creating the App Docker Network

Before you be able to run the application and the karate from its docker instances, it'd be necessary to create the docker network to share the environments. Run bellow command once, it the them fail to start:

```
docker network create aisoft_net
```

### Test Environment

All test environment can be up with docker. Go to the project's root folder an run the following command:

```
docker-compose up
```

This will run the database and the application from its docker images. 

> For the first time, it would need to create the application container from the Dockerfile in the project's root folder. So, it could take a while, dependind on the current network performance.

### Running Integration/Acceptance Tests

The Integration tests for the API is done by Karate framework from provided feature files. 

> Create new feature files in `/integration/features` folder.

To run the tests, after having the project up and running, run the following command inside `/integration` folder.

```
docker-compose up
```

The resulting reportes will be available at `/integration/reports` folder.

> For the first time, it would need to create the karate container from the Dockerfile in the integration folder. So, it could take a while, dependind on the current network performance.

## Creating Custom Services

Custom services are deployed as files in `/services` folder and must call AppRegister to register itself in the application.

> All custom services are available at `[route]` and can override all other default service routes from controllers

#### Route Path Parameters

Route path parameters follows Flask framework standards, like following:

```
def Service(id):
  ...

AppRegister.registerService("test/<int:id>", callback=Service, methods=['GET'])
```

#### Simple Services

Any service will require following imports:

```
from daos.data_dao import DataDao
from lib.register import AppRegister

def Service():
  dao = DataDao()
  ...

AppRegister.registerService("[service route]", callback=Service, methods=['GET'])
```

#### Transactional Services 

```
from daos.data_dao import DataDao
from lib.register import AppRegister
from db.query import Transactional

@Transactional
def Service(tx):
  dao = DataDao()

  dao['method'](tx=tx, ...)
  dao['other_method'](tx=tx, ...)
  ...

AppRegister.registerService("[service route]", callback=Service, methods=['GET'])
```

#### Secured Services 

```
from daos.data_dao import DataDao
from lib.register import AppRegister
from decorators.secured import Secured

@Secured
def Service(currentUser):
  dao = DataDao()
  userId = currentUser.get('id')
  ...

AppRegister.registerService("[service route]", callback=Service, methods=['GET'])
```

## Validators

Validations uses Cerberus library and can be added from `/services/validators.py` file:

```
AppRegister.registerValidator('test', {
    'name': {
        'required': True
    }
})
```

Some optional parameters are:

1. allow_unknown: To disregard unknown fields (defatuls to true)
2. methods: To use that validator with the methods used (defaults to all)

To validate an object, use:

```
AppRegister.validate('test', {'name': 'teste'})
```

Some optional parameters are:

1. method: To select with validator will be used based on the method.

If the validation fails an exception will be raised and one object returned to the caller, with a 400 error code.
