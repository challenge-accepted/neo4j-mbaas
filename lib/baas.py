from daos.data_dao import DataDao

class Baas():

  @property
  def node(self, id=None, owner=None, data={}):
    dao = DataDao()

    return dict(
      list = dao.filter,
      ins = dao.filterIns,
      outs = dao.filterOuts,
      fetch = dao.get,
      create = dao.create,
      merge = dao.merge,
      update = dao.update,
      delete = dao.delete
    )

  @property
  def relation(self, start=None, end=None):
    pass