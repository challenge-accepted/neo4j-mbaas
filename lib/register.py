from cerberus import Validator
from flask import jsonify, abort, make_response

class AppRegister():
  app = None
  validators = {}

  @staticmethod
  def registerController(controller):
    controller(AppRegister.app)

  @staticmethod
  def registerService(route, callback, methods=['GET']):
    # decorates callback with route/methods flask directives
    AppRegister.app.route("%s" % ("/%s" % route if not route.startswith("/") else route), 
      methods=methods)(callback)

  @staticmethod
  def registerValidator(label, schema, allow_unknown=True, methods=['post', 'put', 'merge']):
    AppRegister.validators[label] = {
      'schema': schema,
      'allow_unknown': allow_unknown,
      'methods': [method.lower() for method in methods]
    }

  @staticmethod
  def validate(label, data, method='post'):
    validator = AppRegister.validators.get(label, {})

    if method.lower() in validator.get('methods', []):
      v = Validator(validator.get('schema', {}), allow_unknown=validator.get('allow_unknown', True))

      v.validate(data)

      if v.errors:
        abort(make_response(jsonify(errors=v.errors), 400))

  @staticmethod
  def runTrigger(hook, *args, **kwargs):
    if hook in AppRegister.triggers:
      for trigger in AppRegister.triggers.get(hook):
        trigger(*args, **kwargs)