import json

class BaseModel():
    
    def __init__(self):
        self.payload = dict()
        
    def add(self, key, value):
        if isinstance(value, BaseModel):
            value = value.get_payload()
            
        self.payload[key] = value

    def get(self, key):
        if key in self.payload:
            return self.payload[key]

        return None

    def delete(self, key):
        if key in self.payload:
            del self.payload[key]

    def add_from_record(self, record):
        for key in record.keys():
            self.add(key, record.get(key))
        
    def set_payload(self, data=dict()):
        self.payload = data

    def get_payload(self):
        return self.payload
        
    def json(self):
        return json.dumps(self.payload)

    def __str__(self):
        return self.json()
