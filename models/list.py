from models.base import BaseModel

class BaseList():

    def __init__(self):
        self.payload = []

    def is_empty(self):
        return len(self.payload) == 0

    def add(self, model):
        if isinstance(model, BaseModel):
            model = model.get_payload()

        self.payload.append(model)

    def get_payload(self):
        return self.payload