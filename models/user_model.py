import hashlib
import jwt
import settings
from models.base import BaseModel

class UserModel(BaseModel):

    def simple_user(self):
        self.delete("password")
        self.delete("active")

    def public_user(self):
        self.simple_user()

        # more clean for public view
        self.delete("token")

    @staticmethod
    def generatePassword(plainText):
        byteContent = plainText.encode('utf-8')
        hash = hashlib.sha1(byteContent).hexdigest()

        return hash

    def generateToken(self):
        token = jwt.encode({
            "user": self.get('id')
        }, settings.general['token_secret'], algorithm='HS256')

        return token.decode('utf-8')
