from functools import wraps
import inspect

def Private(method):
  @wraps(method)
  def wrapper(*args, **kwargs):
    context = inspect.stack()[1][0]
    if 'self' not in context.f_locals or context.f_locals['self'] is not args[0]:
      raise Exception("%s.%s is a private method." % (args[0].__class__.__name__, method.__name__))
    
    return method(*args, **kwargs)
    
  return wrapper