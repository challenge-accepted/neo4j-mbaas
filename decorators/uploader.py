import os
from base64 import b64decode
from flask import request, abort
from werkzeug import secure_filename
from functools import wraps
from utils.file.file import FileUtils
import settings

def Uploader(app):

    def uploadHandler(method):
        @wraps(method)
        def innerHandler(*args, **kwargs):
            if 'file' in request.files:
                file = request.files['file']
                fileName = secure_filename(file.filename)
                filePath = os.path.join(app.config['UPLOAD_FOLDER'], fileName)

                # save temp file
                file.save(filePath)
            else:
                try:
                    body = request.json

                    if 'file' not in body or 'file_name' not in body:
                        print('NO parameters')
                        abort(400, 'No file specified.')

                    # fetch file content
                    fileContent = body['file']
                    fileName = body['file_name']
                    filePath = os.path.join(app.config['UPLOAD_FOLDER'], fileName)

                    with open(filePath, "wb") as fh:
                        fh.write(b64decode(fileContent))
                except Exception as ex:
                    print(ex)
                    abort(400, 'No file specified.')

            meta = dict()

            try:
                drive = FileUtils.getDrive()

                # upload new picture
                meta = drive.upload(fileName, uri=filePath)
            except Exception as ex:
                print(ex)
                abort(500)

            try:
                # call handler
                result = method(meta=meta, *args, **kwargs)

            except Exception as ex:
                print(ex)

                # remove previously uploaded file
                drive.delete(meta['fileId'])

                abort(ex)
            finally:
                # remove temp file
                os.remove(filePath)

            # return to flask router
            return result
            
        return innerHandler
    
    return uploadHandler