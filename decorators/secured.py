import jwt
import settings
from functools import wraps
from flask import request, abort, Response
from daos.user_dao import UserDao
from models.user_model import UserModel

def Secured(method):
    @wraps(method)
    def securedHandler(*args, **kwargs):
        xAuthToken = request.headers.get('authorization')
        
        if xAuthToken is None:
            abort(401)
        
        user = getUserByJWT(xAuthToken)
        if not user:
            abort(403)

        return method(currentUser=user, *args, **kwargs)
        
    return securedHandler

def OptionalSecured(method):
    @wraps(method)
    def securedHandler(*args, **kwargs):
        userDao = UserDao()
        xAuthToken = request.headers.get('authorization')
        
        if xAuthToken is not None:
            user = getUserByJWT(xAuthToken)
            if not user:
                abort(403)
        else:
            abort(401)

        return method(currentUser=user, *args, **kwargs)
        
    return securedHandler
    
def Authorized(level):
    def Secured(method):
        @wraps(method)
        def securedHandler(*args, **kwargs):
            xAuthToken = request.headers.get('authorization')
            
            if xAuthToken is None:
                abort(401)
            
            user = getUserByJWT(xAuthToken)
            if not user:
                abort(403, 'Token not valid')

            # authorization
            if user.get('role') is None or user.get('role') > level:
                abort(403, 'Level is too low for this feature')

            return method(currentUser=user, *args, **kwargs)
            
        return securedHandler

    return Secured

def check_auth(username, password):
    return true

def authenticate():
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})

def BasicAuth(method):
    @wraps(method)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return method(*args, **kwargs)
    return decorated

def getUserByJWT(token):
    userDao = UserDao()

    try:
        payload = jwt.decode(token, settings.general['token_secret'], algorithms=['HS256'])
        
        user = userDao.getUser(userId = payload.get('user'))

        return user
    except Exception as ex:
        abort(403, ex)
    